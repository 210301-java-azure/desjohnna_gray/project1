import dev.gray.MainApp;
import dev.gray.objects.Timesheet;
import kong.unirest.GenericType;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;

public class TimesheetIntegrationTest {

        private static MainApp app = new MainApp();

        @BeforeAll
        public static void startService(){
            app.start(80);
        }

        @AfterAll
        public static void stopService(){
            app.stop();
        }

        @Test
        public void testGetMasterTimesheetUnauthorized(){
            HttpResponse<String> response = Unirest.get("http://52.255.169.221/timesheets").asString();
            assertAll(
                    ()->assertEquals( 401,response.getStatus()),
                    ()->assertEquals( "Unauthorized",response.getBody()));
        }

        @Test
        public void testGetMasterTimesheetAuthorized(){
            HttpResponse<List<Timesheet>> response = Unirest.get("http://localhost:7000/items")
                    .header("Authorization", "admin-auth-token")
                    .asObject(new GenericType<List<Timesheet>>() {});
            assertAll(
                    ()->assertEquals(200,response.getStatus()),
                    ()->assertTrue(response.getBody().size()>0)
            );
        }




    }
