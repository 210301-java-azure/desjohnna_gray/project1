package dev.gray.services;

import dev.gray.dao.TimesheetDao;
import dev.gray.dao.TimesheetDaoImpl;
import dev.gray.objects.Timesheet;

import java.util.List;

public class TimesheetService {

    private final TimesheetDao timesheetDao = new TimesheetDaoImpl();

    public List<Timesheet> getMasterTimesheetList() {
        return timesheetDao.getMasterTimesheetList();
    }

    public Timesheet addNewTimesheet(Timesheet newTimesheet) {
        return timesheetDao.addNewTimesheet(newTimesheet);
    }

    public void deleteTimesheet(int timesheetId) {
        timesheetDao.deleteTimesheet(timesheetId);
    }

    public Timesheet getTimesheetById(int id) {
        return timesheetDao.getTimesheetById(id);
    }

    public List<Timesheet> getTimesheetsByEmployeeId(int id) {
        return timesheetDao.getTimesheetsByEmployeeId(id);
    }

}

