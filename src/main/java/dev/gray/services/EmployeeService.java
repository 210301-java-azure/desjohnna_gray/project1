package dev.gray.services;

import dev.gray.dao.EmployeeDao;
import dev.gray.dao.EmployeeDoaImpl;
import dev.gray.objects.Employee;
import dev.gray.objects.Timesheet;

import java.util.List;


public class EmployeeService {

    private EmployeeDao employeeDao = new EmployeeDoaImpl();

    public List<Employee> getMasterEmployeeList() {
        return employeeDao.getMasterEmployeeList();
    }

    public Employee getEmployeeById(int idInput) {
        return employeeDao.getEmployeeById(idInput);
    }

    public Employee addNewEmployee(Employee newEmployee) {
        return employeeDao.addNewEmployee(newEmployee);
    }
}
