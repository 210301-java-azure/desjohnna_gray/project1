package dev.gray;

import io.javalin.core.JavalinConfig;

public class Driver {
    public static void main(String[] args) {

        MainApp javalinApp = new MainApp();
        javalinApp.start(80);

    }

}
