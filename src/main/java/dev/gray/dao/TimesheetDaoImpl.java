package dev.gray.dao;

import dev.gray.objects.Timesheet;
import dev.gray.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class TimesheetDaoImpl implements TimesheetDao {

    private final Logger logger = LoggerFactory.getLogger(TimesheetDaoImpl.class);

    @Override
    public Timesheet addNewTimesheet(Timesheet newTimesheet) {
        try (Session s = HibernateUtil.getSession()) {
            // wrapping transaction around this
            Transaction tx = s.beginTransaction();

            int id = (int) s.save(newTimesheet);
            newTimesheet.setTimesheetId(id);
            logger.info("saving new timesheet to db");
            tx.commit();
            return newTimesheet;

        }

    }

    @Override
    public List<Timesheet> getMasterTimesheetList() {
        try (Session s = HibernateUtil.getSession()) {
            logger.info("getting master timesheet list from db");
            return s.createQuery("from Timesheet", Timesheet.class).list();
        }
    }

    @Override
    public Timesheet getTimesheetById(int id) {
        try (Session s = HibernateUtil.getSession()) {
//returns object if exists in db if not returns null
            logger.info("getting time sheet by id");
            return s.get(Timesheet.class, id);
        }
    }

    @Override
    public void deleteTimesheet(int id) {
        try (Session s = HibernateUtil.getSession()) {
            Transaction tx = s.beginTransaction();

            s.delete(new Timesheet(id));

            logger.info("saving new timesheet to db");
            tx.commit();

        }
    }

    @Override
    public List<Timesheet> getTimesheetsByEmployeeId(int employeeId) {
        try (Session s = HibernateUtil.getSession()) {
//try to use load here
//            Query q1 = s.load(Timesheet.class, employeeId);

            Query<Timesheet> timesheetQuery = s.createQuery("SELECT E.employeeId FROM Timesheet T", Timesheet.class);
            timesheetQuery.setParameter("employeeId", employeeId);

            return timesheetQuery.list();
        }
    }

    @Override
    public Timesheet updateTimesheet(Timesheet timesheet) {
        try (Session s = HibernateUtil.getSession()) {
            Transaction tx = s.beginTransaction();
            s.saveOrUpdate(timesheet);
            tx.commit();
            return timesheet;
        }
    }
}
