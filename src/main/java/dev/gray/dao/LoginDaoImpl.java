package dev.gray.dao;

import dev.gray.objects.Employee;
import dev.gray.util.HibernateUtil;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoginDaoImpl implements LoginDao {
    Logger logger = LoggerFactory.getLogger(LoginDaoImpl.class);

    @Override
    public Employee GetEmployeeLogin(int id) {
        try (Session s = HibernateUtil.getSession()) {
            logger.info("getting employee login info");
            return s.get(Employee.class, id);
        }

    }
}