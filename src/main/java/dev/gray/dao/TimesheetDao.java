package dev.gray.dao;

import dev.gray.objects.Timesheet;

import java.util.List;

public interface TimesheetDao {


    Timesheet addNewTimesheet(Timesheet newTimesheet);

    //Admin Only Methods
    List<Timesheet> getMasterTimesheetList();

    Timesheet getTimesheetById(int id);

    void deleteTimesheet(int id);

    List<Timesheet> getTimesheetsByEmployeeId(int employeeId);

    Timesheet updateTimesheet(Timesheet timesheet);

}
