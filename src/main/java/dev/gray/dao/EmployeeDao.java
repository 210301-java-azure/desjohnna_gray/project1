package dev.gray.dao;

import dev.gray.objects.Employee;

import java.util.List;

public interface EmployeeDao {

    Employee getEmployeeById(int id);
    Employee addNewEmployee(Employee employee);
    List<Employee> getMasterEmployeeList();

}
