package dev.gray.dao;

import dev.gray.objects.Employee;
import dev.gray.objects.Timesheet;
import dev.gray.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class EmployeeDoaImpl implements EmployeeDao {

    Logger logger = LoggerFactory.getLogger(EmployeeDoaImpl.class);

    @Override
    public Employee getEmployeeById(int id) {
//        try (Session s = HibernateUtil.getSession()) {
//            return s.createQuery("from Employee", Employee.class).getSingleResult();

        try (Session s = HibernateUtil.getSession()) {
            logger.info("getting employee by id");
            return s.get(Employee.class, id);
        }

    }

    @Override
    public List<Employee> getMasterEmployeeList() {
        try (Session s = HibernateUtil.getSession()) {
            logger.info("getting master employee list");
            return s.createQuery("from Employee", Employee.class).list();
        }
    }
    @Override
    public Employee addNewEmployee(Employee newEmployee) {
        try (Session s = HibernateUtil.getSession()) {

            Transaction tx = s.beginTransaction();
            int id = (int) s.save(newEmployee);
            newEmployee.setEmployeeId(id);
            logger.info("saving new timesheet to db");
            tx.commit();
            return newEmployee;

        }
    }
}
