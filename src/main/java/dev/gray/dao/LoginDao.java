package dev.gray.dao;

import dev.gray.objects.Employee;

public interface LoginDao {

    Employee GetEmployeeLogin(int id);
}
