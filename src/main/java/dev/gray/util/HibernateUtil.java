package dev.gray.util;

import dev.gray.objects.Employee;
import dev.gray.objects.Timesheet;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;

import java.util.Properties;

public class HibernateUtil {
    private static SessionFactory sessionFactory;

    private static SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            Configuration configuration = new Configuration();
            Properties settings = new Properties();
//            settings.put(Environment.URL, System.getenv("connectionUrl"));
//            settings.put(Environment.USER, System.getenv("username"));
//            settings.put(Environment.PASS, System.getenv("password"));

            settings.put(Environment.URL, "jdbc:sqlserver://project1-gray.database.windows.net:1433;databaseName=project1");
            settings.put(Environment.USER, "dgray@project1-gray");
            settings.put(Environment.PASS, "Deonna77");

            //https://docs.microsoft.com/en-us/sql/connect/jdbc/working-with-a-connection?view=sql-server-ver15
            settings.put(Environment.DRIVER, "com.microsoft.sqlserver.jdbc.SQLServerDriver");
            //https://www.javatpoint.com/dialects-in-hibernate
            settings.put(Environment.DIALECT, "org.hibernate.dialect.SQLServerDialect");

            settings.put(Environment.HBM2DDL_AUTO, "validate"); //create here created our tables or validate to run
            settings.put(Environment.SHOW_SQL, "true");

            configuration.setProperties(settings);

            //provide hibernate mappings to configuration
            configuration.addAnnotatedClass(Timesheet.class);
            configuration.addAnnotatedClass(Employee.class);


            sessionFactory = configuration.buildSessionFactory();
        }
        return sessionFactory;
    }


    public static Session getSession() {
        return getSessionFactory().openSession();
    }


}
