package dev.gray.util;

import io.javalin.http.Context;

public class SecurityUtil {

    public void attachResponseHeaders(Context ctx){
        ctx.header("Access-Control-Expose-Headers", "Authorization");
//        ctx.header("Access-Control-Allow-Origin", "*"); // this is what the JavalinConfig::enableCorsForAllOrigins
    }

}
