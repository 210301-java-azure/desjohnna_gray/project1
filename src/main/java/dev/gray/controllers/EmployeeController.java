package dev.gray.controllers;


import dev.gray.objects.Employee;
import dev.gray.objects.Timesheet;
import dev.gray.services.EmployeeService;

import io.javalin.http.Context;
import io.javalin.http.UnauthorizedResponse;


public class EmployeeController {

    private EmployeeService employeeService = new EmployeeService();


    public void login(Context ctx) {

        String userIdInput = ctx.formParam("username");
        String passwordInput = ctx.formParam("password");
//        String admin = ctx.queryParam("admin");

        if (userIdInput != null && userIdInput.equals("username")) {
            if (passwordInput != null && passwordInput.equals("password")) {

                ctx.header("Authorization", "admin-auth-token");
                ctx.status(200);
                return;

            }
            throw new UnauthorizedResponse("Please enter correct Username and Password");
        }
    }

    public void getEmployeeByUserId(Context ctx) {
        String userIdString = ctx.pathParam("id");

        int idInput = Integer.parseInt(userIdString);

        Employee employee = employeeService.getEmployeeById(idInput);
        ctx.json(employee);
        ctx.status(200);

    }

    public void getMasterEmployeeList(Context ctx) {
        ctx.json(employeeService.getMasterEmployeeList());
    }

    public void addNewEmployee(Context ctx) {

        Employee employee = ctx.bodyAsClass(Employee.class);
        ctx.json(employeeService.addNewEmployee(employee));
        ctx.status(201);
    }
}
