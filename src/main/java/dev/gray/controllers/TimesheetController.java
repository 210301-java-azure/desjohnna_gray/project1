package dev.gray.controllers;

import dev.gray.objects.Timesheet;
import dev.gray.services.TimesheetService;
import io.javalin.http.Context;


import java.util.List;


public class TimesheetController {

    private TimesheetService timesheetService = new TimesheetService();

    public TimesheetController() {
        super();
    }

    public void getMasterTimesheetList(Context ctx) {
        ctx.json(timesheetService.getMasterTimesheetList());
    }


    public void addNewTimesheet(Context ctx) {
//    Add logic to allow access during specific times of day only if/else throw IllegalArgumentException
        Timesheet timesheet = ctx.bodyAsClass(Timesheet.class);
        ctx.json(timesheetService.addNewTimesheet(timesheet));
        ctx.status(201);
    }

    public void deleteTimesheet(Context ctx) {
//        Declaring and Parsing the String id into an int
        String idString = ctx.pathParam("id");
        int idInput = Integer.parseInt(idString);
        timesheetService.deleteTimesheet(idInput);
//            Send GONE status to confirm deletion
        ctx.status(410);
    }


    public void getTimesheetsByEmployeeId(Context ctx) {
//      CREATING A VARIABLE TO HOLD THE PATH PARAMETER ID IN
        String idString = ctx.pathParam("id");
//      PARSING ID FROM A STRING TO AN INT
        int idInput = Integer.parseInt(idString);
//      CREATING A LIST TO PUT THE LOGS MATCHING THE USER ID IN
        List<Timesheet> userLog = timesheetService.getTimesheetsByEmployeeId(idInput);
//      DISPLAYING THE LIST OF LOGS IN JSON FORMAT
        ctx.json(userLog);
        ctx.status(200);
    }

    public void getTimesheetById(Context ctx) {
//      CREATING A VARIABLE TO HOLD THE PATH PARAMETER ID IN
        String idString = ctx.pathParam("id");
//      PARSING ID FROM A STRING TO AN INT
        int idInput = Integer.parseInt(idString);
//      CREATING A LOG OBJECT TO PUT THE LOG MATCHING THE ENTRY ID IN
        Timesheet timesheet = timesheetService.getTimesheetById(idInput);
//      DISPLAYING THE LIST OF LOGS IN JSON FORMAT
        ctx.json(timesheet);
        ctx.status(200);

    }
}

