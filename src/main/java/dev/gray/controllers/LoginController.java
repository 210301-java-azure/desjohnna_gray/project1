package dev.gray.controllers;

import dev.gray.dao.LoginDaoImpl;
import dev.gray.objects.Employee;
import dev.gray.services.LoginService;
import io.javalin.http.Context;
import io.javalin.http.ForbiddenResponse;
import io.javalin.http.UnauthorizedResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.Objects;


public class LoginController {

    private LoginService loginService = new LoginService();
    private final Logger logger = LoggerFactory.getLogger(LoginController.class);


    public void authLogin(Context ctx) throws SQLException {

        int employeeId = Integer.parseInt(Objects.requireNonNull(ctx.formParam("employeeId")));
        String password = ctx.formParam("password");
        logger.info("Attempting to login employee {} from form param inputs ", employeeId);
//        Employee employee = new Employee(employeeId, password);

//        Getting the employee object from login service and setting the Id as a variable
        int idInDatabase = loginService.getEmployeeLogin(employeeId).getId();

//        Getting the employee object from login service and setting the password as a variable
        String passwordInDatabase = loginService.getEmployeeLogin(employeeId).getPassword();

        boolean admin = loginService.getEmployeeLogin(employeeId).getAdmin();

        String authHeader = "Authorization";

//        Checking if id matches, then making sure the password isn't null and matches the password in database
        if (employeeId == idInDatabase) {
            assert password != null;
            if (password.equals(passwordInDatabase)) {
                logger.info("successful login");
                if (admin) {
                    ctx.header(authHeader, "admin-auth-token");
                } else {
                    ctx.header(authHeader, "employee-auth-token");
                }
                ctx.status(200);
            } else {
                logger.warn("incorrect PASSWORD throwing unauthorized response");
                throw new UnauthorizedResponse("Password incorrect");

            }
        } else {
            logger.warn("incorrect EMPLOYEE ID throwing unauthorized response");

            throw new UnauthorizedResponse("Employee Id not recognized");
        }


    }


    public void authorizeToken(Context ctx) {
        logger.info("attempting to authorize token");

        if (ctx.method().equals("OPTIONS")) {
            return;
        }

        String authHeader = ctx.header("Authorization");
        if (authHeader != null && (authHeader.equals("admin-auth-token") || authHeader.equals("employee-auth-token"))){
            logger.info("request is authorized, proceeding to handler method");
        } else {
            logger.warn("improper authorization");
            throw new ForbiddenResponse();
        }
    }

}

