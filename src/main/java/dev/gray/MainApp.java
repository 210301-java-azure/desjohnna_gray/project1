package dev.gray;

import dev.gray.controllers.TimesheetController;
import dev.gray.controllers.EmployeeController;
import dev.gray.controllers.LoginController;
import dev.gray.util.SecurityUtil;
import io.javalin.Javalin;
import io.javalin.core.JavalinConfig;

import static io.javalin.apibuilder.ApiBuilder.*;

public class MainApp {

    //      Creating Controllers to call in routes
    TimesheetController timesheetController = new TimesheetController();
    EmployeeController employeeController = new EmployeeController();
    LoginController loginController = new LoginController();
    SecurityUtil securityUtil = new SecurityUtil();

    //      CREATING JAVALIN APP TO RUN ON LOCALHOST:7000
    Javalin app = Javalin.create(JavalinConfig::enableCorsForAllOrigins).routes(() -> {

//       Timesheet Routes for Admins
        path("/timesheets", () -> {
            before("/", loginController::authorizeToken);
            get(timesheetController::getMasterTimesheetList);
            post(timesheetController::addNewTimesheet);
            path(":id", () -> {
                before("/", loginController::authorizeToken);
                get(timesheetController::getTimesheetById);
                delete(timesheetController::deleteTimesheet);
            });
        });
//       Timesheet Routes for employees
        path("/timesheets/id/:employeeId", () -> {
                before("/", loginController::authorizeToken);
                get(timesheetController::getTimesheetsByEmployeeId);
        });

//         Login Routes
        path("/login", () -> {
            post(loginController::authLogin);
            after("/", securityUtil::attachResponseHeaders);
        });


//        Employee Routes
        path("employees", () -> {
            before("/", loginController::authorizeToken);
            get(employeeController::getMasterEmployeeList);
            post(employeeController::addNewEmployee);
            path(":id", () -> get(employeeController::getEmployeeByUserId));
        });
    });


    public void start(int port) {
        this.app.start(port);
    }

    public void stop() {
        this.app.stop();
    }


}



