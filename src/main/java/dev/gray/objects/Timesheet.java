package dev.gray.objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.util.Objects;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "timesheet")
public class Timesheet implements Serializable {
    //  Setting the timesheetId as the primary key and to auto generate its value
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    // Setting employeeId as the foreign key to the employeeId primary key column of the employee table

    @JoinColumn(name = "employeeId")
    private Integer employeeId;
    private String submitDate;
    private double ptoHours;
    private double regHours;
    private String comment = " ";

    public Timesheet() {
        super();
    }

    public Timesheet(Integer id) {
        this.id = id;
    }

    public Timesheet(Integer id, Integer employeeId, String submitDate, double ptoHours, double regHours, String comment) {
        this.id = id;
        this.employeeId = employeeId;
        this.submitDate = submitDate;
        this.ptoHours = ptoHours;
        this.regHours = regHours;
        this.comment = comment;
    }

    public Timesheet(Integer employeeId, String submitDate, double ptoHours, double regHours, String comment) {
        this.employeeId = employeeId;
        this.submitDate = submitDate;
        this.ptoHours = ptoHours;
        this.regHours = regHours;
        this.comment = comment;
    }

    public Integer getId(int id) {
        return id;
    }

    public void setTimesheetId(Integer id) {
        this.id = id;
    }

    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    public String getSubmitDate() {
        return submitDate;
    }

    public void setSubmitDate(String submitDate) {
        this.submitDate = submitDate;
    }


    public double getPtoHours() {
        return ptoHours;
    }

    public void setPtoHours(double ptoHours) {
        this.ptoHours = ptoHours;
    }

    public double getRegHours() {
        return regHours;
    }

    public void setRegHours(double regHours) {
        this.regHours = regHours;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Timesheet timesheet = (Timesheet) o;
        return Double.compare(timesheet.ptoHours, ptoHours) == 0 && Double.compare(timesheet.regHours, regHours) == 0 && id.equals(timesheet.id) && employeeId.equals(timesheet.employeeId) && submitDate.equals(timesheet.submitDate) && Objects.equals(comment, timesheet.comment);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, employeeId, submitDate, ptoHours, regHours, comment);
    }

    @Override
    public String toString() {
        return "Timesheet{" +
                "timesheetId=" + id +
                ", employeeId=" + employeeId +
                ", submitDate=" + submitDate +
                ", ptoHours=" + ptoHours +
                ", regHours=" + regHours +
                ", comment='" + comment + '\'' +
                '}';
    }
}
