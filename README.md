# Cafe Latte Employee Portal

##Project Description

The Cafe Latte website that customers can visit to view information about the coffee shop. Employees have the ability to login to an EmployeeCafe Portal where they can then submit new timesheets and search for past timesheets. Admins have the same abilities as employees in the AdminCafe Portal and Admins can also view masterlists, search and create new timesheets and employees.
<!-- ![Screenshot](LoginSnip.png) -->

## Technologies

---

A list of technologies used within the project:

- [Java](https://docs.oracle.com/javase/8/): Version 1.8.0_211
- [Hibernate](https://hibernate.org/orm/documentation/5.4/): Version 5.4.30
- [Jenkins](https://www.jenkins.io/doc/): Version 2.277.1
- [Ubuntu](https://help.ubuntu.com): Version 18.04.5
- [Javalin](https://javalin.io/): Version 3.13.3
- [PostgreSql](https://www.postgresql.org/docs/): Version 42.2.18
- [Azure Blob](https://azure.microsoft.com/en-us/services/storage/blobs/)

## Features

- Responsive design with Bootstrap created by Start Bootstrap.
- Landing page, about page, products page, contact page, administration portal and employee portal.

- Login Features:

  - Employees can login to the EmployeeCafe portal.
  - Admins can log into either the EmployeeCafe or AdminCafe.
  - Users are only allowed access to the pages allowed by Based on their designated authentication token.
  - There is an auto-logout timer of 10 minutes unless the logout button is manually clicked.

- Admins can:

  - Get Master Timesheet List
  - Get Timesheets By Id
  - Create New Timesheets
  - Get Master Employee List
  - Get Employee By Id
  - Create New Employee

- Employees can:

  - Create new timesheets

#### To-do list:

- Profile page with ability to update employee password
- Allow employees to see a list of thier past imesheets

## Getting Started

GitLab Clone: (https://gitlab.com/210301-java-azure/desjohnna_gray/project1.git)

<!-- (i (include all environment setup steps)

Be sure to include BOTH Windows and Unix command
Be sure to mention if the commands only work on a specific platform (eg. AWS, GCP)

All the code required to get started
Images of what it should look like -->

- Frontend is deployed to Azure Blob:(https://djgrayblob05.blob.core.windows.net/graystorage/html/index.html)
- Backend is deployed to Azure Linux Virtual Machine: (http://52.255.169.221:8080/job/timesheet/)

<!-- Usage
Here, you instruct other people on how to use your project after they’ve installed it. This would also be a good place to include screenshots of your project in action.
 -->

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE) file for details.

- Bootstrap Copyright:
  - Start Bootstrap is an open source library of free Bootstrap templates and themes.
    - Copyright 2013-2021 Start Bootstrap LLC.

## Acknowledgments

- Start Bootstrap Buisness Casual Theme
- Carolyn
- Co-horts
