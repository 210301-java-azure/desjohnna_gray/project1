
function sendAjaxRequest(method, url, body, successCallback, failureCallback, authToken) {
    const xhr = new XMLHttpRequest();
    xhr.open(method, url);
    if (authToken) {
        xhr.setRequestHeader("Authorization", authToken);
    }
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            if (xhr.status > 199 && xhr.status < 300) {
                successCallback(xhr);
            } else {
                failureCallback(xhr);
            }
        }
    }
    if (body) {
        xhr.send(body);
    } else {
        xhr.send();
    }
}


function sendAjaxPost(url, body, successCallback, failureCallback, authToken) {
    sendAjaxRequest("POST", url, body, successCallback, failureCallback, authToken);
}

// Login Requests
function ajaxLogin(employeeId, password, successCallback, failureCallback) {
    const payload = `employeeId=${employeeId}&password=${password}`; 

    sendAjaxPost("http://52.255.169.221/login", payload, successCallback, failureCallback);
}

// Timesheet Requests

// Create New Timesheet - Admin Only**
function ajaxCreateTimesheet(timesheet, successCallback, failureCallback) {
    const timesheetJson = JSON.stringify(timesheet);
    console.log(sessionStorage.getItem("token"));
    const auth = sessionStorage.getItem("token");
    sendAjaxPost("http://52.255.169.221/timesheets", timesheetJson, successCallback, failureCallback, auth);
}

// Employee Requests

// Create New Employee - Admin Only**
function ajaxCreateEmployee(employee, successCallback, failureCallback) {
    const employeeJson = JSON.stringify(employee);
    const auth = sessionStorage.getItem("token");
    sendAjaxPost("http://52.255.169.221/employees", employeeJson, successCallback, failureCallback, auth);
}
