document.getElementById("login-form").addEventListener("submit", attemptLogin);



function attemptLogin(event) {
    event.preventDefault();// this stops page from refreshing after submiting form
    const employeeId = document.getElementById("inputEmployeeId").value;
    const password = document.getElementById("inputPassword").value;

    ajaxLogin(employeeId, password, successfulLogin, loginFailed)

}

function successfulLogin(xhr) {
    console.log("login successful");
    const authToken = xhr.getResponseHeader("Authorization");
   
    sessionStorage.setItem("token", authToken);
     console.log(authToken);
    if (authToken == "admin-auth-token") {
        window.location.href = "adminCafe.html";
    } else {
        window.location.href = "employeeCafe.html";
    }
    // Setting a timer to call the logout function in 10 minutes
    setTimeout(logout, 600000);
}

function loginFailed() {
    console.log("oh no something went wrong");
    const errorDiv = document.getElementById("error-msg");
    errorDiv.hidden = false;
    errorDiv.innerText = "Login failed, please try credentials again."
}

function logout() {
    console.log("successfully logged out");
    sessionStorage.removeItem("token");
    window.location.href = "index.html";

}
