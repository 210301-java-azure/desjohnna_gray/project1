document.getElementById("button-addon2").addEventListener("click", attemptTimesheetById);


function attemptTimesheetById(event) {
    event.preventDefault();
    const tableBody = document.getElementById("timesheet-table-body");
    tableBody.innerHTML = ``;
   
    const id = document.getElementById("employeeIdInput").value;

    // ajaxGetTimesheetById(id, successfulLoad, failedLoadTimesheet);
    console.log(id);





    const xhr3 = new XMLHttpRequest();
    xhr3.open("GET", `http://52.255.169.221/timesheets/${id}`);
    let authToken = sessionStorage.getItem("token");
    xhr3.setRequestHeader("Authorization", authToken);


    xhr3.onreadystatechange = function () {
        if (xhr3.readyState == 4) {
            if (xhr3.status > 199 && xhr3.status < 300) {
                const timesheet = JSON.parse(xhr3.responseText);
                renderTimesheetInTable(timesheet);
            } else {
                failedLoadTimesheet();
                console.log("Get timesheet request failed in timesheetById.js")
            }
        }
    }
    xhr3.send();
}

function renderTimesheetInTable(timesheet) {
    console.log(timesheet);


    // if (authToken == "admin-auth-token") {
    // document.getElementById("timesheet-table").hidden = false;
    // document.getElementById("LoginErrorMsg").hidden = true;
    // } else {
    // document.getElementById("timesheet-table").hidden = true;
    // document.getElementById("LoginErrorMsg").hidden = false;
    // window.location.href = "employeeCafe.html";
    // }


    const tableBody = document.getElementById("timesheet-table-body");


    const newRow = document.createElement("tr");
    let submitDate = new Date(timesheet.submitDate).toLocaleDateString();
    let id = document.getElementById("employeeIdInput").value;
    newRow.innerHTML = `
            <td>${id}</td>
            <td>${timesheet.employeeId}</td>
            <td>${submitDate}</td>
            <td>${timesheet.ptoHours}</td>
            <td>${timesheet.regHours}</td>
            <td>${timesheet.comment}</td>`;
    tableBody.appendChild(newRow);

}

function failedLoadTimesheet() {
    console.log("failed to load timesheet by id");
    const errorDiv = document.getElementById("LoginErrorMsg");
    errorDiv.hidden = false;

}