document.getElementById("button-addon2").addEventListener("click", attemptTimesheetById);


function attemptTimesheetById(event) {
    event.preventDefault();
    const id = document.getElementById("employeeIdInput").value;

    // ajaxGetTimesheetById(id, successfulLoad, failedLoadTimesheet);
    console.log(id);





    const xhr3 = new XMLHttpRequest();
    xhr3.open("GET", `http://52.255.169.221/employees/${id}`);
    const authToken = sessionStorage.getItem("token");
    xhr3.setRequestHeader("Authorization", authToken);


    xhr3.onreadystatechange = function () {
        if (xhr3.readyState == 4) {
            if (xhr3.status > 199 && xhr3.status < 300) {
                const employee = JSON.parse(xhr3.responseText);
                renderEmployeeInTable(employee);
            } else {
                failedLoadTimesheet();
                console.log("Get timesheet request failed in timesheetById.js")
            }
        }
    }
    xhr3.send();
}

function renderEmployeeInTable(employee) {
    console.log(employee);
    const authToken = sessionStorage.getItem("token");

    if (authToken == "admin-auth-token") {
        document.getElementById("employee-table").hidden = false;
    } else {
        document.getElementById("employee-table").hidden = true;
    }


    const tableBody = document.getElementById("employee-table-body");

    const newRow = document.createElement("tr");

    newRow.innerHTML = `
        <td>${employee.id}</td>
        <td>${employee.firstName}</td>
        <td>${employee.lastName}</td>
        <td>${employee.admin}</td>`;
    tableBody.appendChild(newRow);

}

function failedLoadTimesheet() {
    console.log("failed to load timesheet by id");
    const errorDiv = document.getElementById("LoginErrorMsg");
    errorDiv.hidden = false;

}