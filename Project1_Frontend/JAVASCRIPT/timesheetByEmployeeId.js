
const employeeId = document.getElementById("employeeIdInput").value;
document.getElementById("employeeIdInput").addEventListener("submit", renderTimesheetById);




const xhr = new XMLHttpRequest();
xhr.open("GET", "http://52.255.169.221/timesheets/id" + employeeId);
let authToken = sessionStorage.getItem("token");
xhr.setRequestHeader("Authorization", authToken);

// xhr.setRequestHeader("Authorization", "admin-auth-token");

xhr.onreadystatechange = function() {
    if (xhr.readyState == 4) {
        if (xhr.status > 199 && xhr.status < 300) {
            const timesheetList = JSON.parse(xhr.responseText);
            renderTimesheetInTable(timesheetList);
        } else {
            console.log("Get timesheet request failed in timesheet.js")
        }
    }
}
// xhr.send();



// if (token && token == "admin-auth-token") {

function renderTimesheetById(timesheetList) {

    if (authToken) {
        document.getElementById("timesheet-table").hidden = false;
    } else {
        document.getElementById("timesheet-table").hidden = true;
        // render error message
        // window.location.href = "employeeCafe.html";
    }


    const tableBody = document.getElementById("timesheet-table-body");

    for (let timesheet of timesheetList) {
        let newRow = document.createElement("tr");
        submitDate = new Date(timesheet.submitDate).toLocaleDateString();

        newRow.innerHTML = `
            <td>${submitDate}</td>
            <td>${timesheet.ptoHours}</td>
            <td>${timesheet.regHours}</td>
            <td>${timesheet.comment}</td>`;
        tableBody.appendChild(newRow);
    }


}