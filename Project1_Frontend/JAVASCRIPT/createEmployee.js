document.getElementById("submitEmployee").addEventListener("click", addNewEmployee);


function addNewEmployee(event) {
    event.preventDefault();

    const firstName = document.getElementById("first-name").value;

    const lastName = document.getElementById("last-name").value;

    let admin = false;

    if (document.getElementById("admin").checked == true) {
        admin = true;
    } 


    const employee = { "firstName": firstName, "lastName": lastName, "admin": admin};

    ajaxCreateEmployee(employee, indicateSuccess, indicateFailure);

}

function indicateSuccess() {
    const message = document.getElementById("create-msg");
    message.hidden = false;
    message.innerText = "New employee successfully created";
    setTimeout(window.location.href = "timesheet.html", 5000);
  
}

function indicateFailure(xhr) {
    const message = document.getElementById("create-msg");
    message.hidden = false;
    message.innerText = JSON.parse(xhr.responseText);
}

function validateInputs() {
    var yArr = document.getElementsByClassName("checkInput").value;
    var xArr = document.getElementsByClassName("checkPtoInput").value;

    // If x or y is Not a Number or less than one or greater the store allowed max pto and reg hours

    if (isNaN(x) || x < 0 || x > 40 || isNaN(y) || y < 0 || y > 24) {

        text = "Input not valid";

    } else {
        text = "";
    }
    document.getElementById("warnTest").innerHTML = text;

}