
const xhr = new XMLHttpRequest();
xhr.open("GET", "http://52.255.169.221/employees");
let authToken = sessionStorage.getItem("token");

xhr.setRequestHeader("Authorization", authToken);

xhr.onreadystatechange = function () {
    if (xhr.readyState == 4) {
        if (xhr.status > 199 && xhr.status < 300) {
            const employeeList = JSON.parse(xhr.responseText);
            renderEmployeeInTable(employeeList);
        } else {
            console.log("Get timesheet request failed in timesheet.js")
        }
    }
}
xhr.send();

function renderEmployeeInTable(employeeList) {

    if (authToken == "admin-auth-token") {
        document.getElementById("employee-table").hidden = false;
    } else {
        document.getElementById("employee-table").hidden = true;
    }

    const tableBody = document.getElementById("employee-table-body");

    for (let employee of employeeList) {
        let newRow = document.createElement("tr");

        newRow.innerHTML = `
            <td>${employee.id}</td>
            <td>${employee.firstName}</td>
            <td>${employee.lastName}</td>
            <td>${employee.admin}</td>`;

        tableBody.appendChild(newRow);
    }

}

