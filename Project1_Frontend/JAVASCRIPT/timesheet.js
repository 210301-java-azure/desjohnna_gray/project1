
const xhr = new XMLHttpRequest();
xhr.open("GET", "http://52.255.169.221/timesheets");
let authToken = sessionStorage.getItem("token");

xhr.setRequestHeader("Authorization", authToken);

xhr.onreadystatechange = function () {
    if (xhr.readyState == 4) {
        if (xhr.status > 199 && xhr.status < 300) {
            const timesheetList = JSON.parse(xhr.responseText);
            renderTimesheetInTable(timesheetList);
        } else {
            console.log("Get timesheet request failed in timesheet.js")
        }
    }
}
xhr.send();







// if (authToken2 && authToken2 == "admin-auth-token") {

function renderTimesheetInTable(timesheetList) {

    if (authToken == "admin-auth-token") {
        document.getElementById("timesheet-table").hidden = false;
    } else {
        document.getElementById("timesheet-table").hidden = true;
        // render error message

    }


    const tableBody = document.getElementById("timesheet-table-body");

    for (let timesheet of timesheetList) {
        let newRow = document.createElement("tr");

        // const name = employee.name;
        submitDate = new Date(timesheet.submitDate).toLocaleDateString();






        newRow.innerHTML = `
            <td>${timesheet.employeeId}</td>
            <td>${submitDate}</td>
            <td>${timesheet.ptoHours}</td>
            <td>${timesheet.regHours}</td>
            <td>${timesheet.comment}</td>`

        tableBody.appendChild(newRow);
    }

}

